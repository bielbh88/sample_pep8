__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'


class BigSolution(object):
    
    def __init__(self, first_number, second_number):
        super().__init__()
        self.first_number = first_number
        self.second_number = second_number
    
    def find_sum_of_multiples(self, limit):
        first_number_multiples_sum = BigSolution.number_multiples_sum(
            limit, self.first_number
        )
        second_number_multiples_sum = BigSolution.number_multiples_sum(
            limit, self.second_number
        )
        
        product = self.first_number * self.second_number
        product_multiples_sum = BigSolution.number_multiples_sum(
            limit, product
        )
        
        result = (
                first_number_multiples_sum
                + second_number_multiples_sum
                - product_multiples_sum
        )
        return result
    
    @staticmethod
    def number_multiples_sum(limit, number):
        return BigSolution.pa_sum(number, BigSolution.divide(limit, number))
    
    @staticmethod
    def divide(limit, number):
        return (limit - 1) // number
    
    @staticmethod
    def pa_sum(first_element, n_elements):
        return first_element * n_elements * (n_elements + 1) / 2
