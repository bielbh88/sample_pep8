"""This package resolves a complex problem with sum of multiples.

Limited to 10, multiples of 3 or 5 (3, 5, 6, 9) results at 23.
"""

__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'
__all__ = [
    'BigSolution',
    'find_sum_of_multiples',
    'find_sum_of_multiples_of_3_and_5_since_to_1000'
]

from .bigsolution import BigSolution
from .find import (
    find_sum_of_multiples,
    find_sum_of_multiples_of_3_and_5_since_to_1000
)
