__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'

import logging as lg


class BigSolution(object):
    
    def __init__(self, first_number, second_number):
        super().__init__()
        self.first_number = first_number
        self.second_number = second_number
        self.logger = lg.getLogger('MyLogger')
    
    def find_sum_of_multiples(self, limit):
        multiples = list()
        for n in range(limit):
            if n % self.first_number == 0:
                multiples.append(n)
                self.logger.info(
                    f'Found {n} as multiple of {self.first_number}.')
            elif n % self.second_number == 0:
                multiples.append(n)
                self.logger.info(
                    f'Found {n} as multiple of {self.second_number}.')
            else:
                self.logger.info(
                    f'Number {n} is not multiple of neither of '
                    f'{self.first_number} and {self.second_number}.')
        result = sum(multiples)
        return result


class BigSolution(object):
    
    def __init__(self, first_number, second_number):
        super().__init__()
        self.first_number = first_number
        self.second_number = second_number
        self.logger = lg.getLogger('MyLogger')
    
    def find_sum_of_multiples(self, limit):
        multiples = list()
        for n in range(limit):
            if n % self.first_number == 0:
                multiples.append(n); self.logger.info('...')
            elif n % self.second_number == 0:
                multiples.append(n); self.logger.info('...')
            else: self.logger.info('...')
        result = sum(multiples)
        return result


class BigSolution(object):
    
    def __init__(self, first_number, second_number):
        super().__init__()
        self.first_number = first_number
        self.second_number = second_number
        self.logger = lg.getLogger('MyLogger')
    
    def match(self, n):
        return n % self.first_number == 0 or n % self.second_number == 0
    
    def find_sum_of_multiples(self, limit):
        multiples = list()
        for n in filter(self.match, range(limit)): multiples.append(n)
        result = sum(multiples)
        return result


class BigSolution(object):
    
    def __init__(self, first_number, second_number):
        super().__init__()
        self.first_number = first_number
        self.second_number = second_number
        self.logger = lg.getLogger('MyLogger')
    
    def match(self, n):
        return n % self.first_number == 0 or n % self.second_number == 0
    
    def find(self, limit): return sum(filter(self.match, range(limit)))
