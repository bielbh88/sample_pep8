__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'

import logging as lg


class BigSolution(object):
    
    logger = lg.getLogger('BigSolution')
    
    def __init__(self, first_number, second_number):
        super().__init__()
        self.first_number = first_number
        self.second_number = second_number
    
    def find_sum_of_multiples(self, limit):
        first_number_multiples_sum = BigSolution.number_multiples_sum(
            limit, self.first_number
        )
        self.log(
            f'First number multiples got sum {first_number_multiples_sum}.'
        )
        
        second_number_multiples_sum = BigSolution.number_multiples_sum(
            limit, self.second_number
        )
        self.log(
            f'Second number multiples got sum {second_number_multiples_sum}.'
        )
        
        product = self.first_number * self.second_number
        product_multiples_sum = BigSolution.number_multiples_sum(
            limit, product
        )
        self.log(f'Product multiples got sum {product_multiples_sum}.')
        
        result = (
                first_number_multiples_sum
                + second_number_multiples_sum
                - product_multiples_sum
        )
        self.log(f'Final result is {result}.')
        
        return result
    
    @staticmethod
    def number_multiples_sum(limit, number):
        return BigSolution.pa_sum(number, BigSolution.divide(limit, number))
    
    @staticmethod
    def divide(limit, number):
        return (limit - 1) // number
    
    @staticmethod
    def pa_sum(first_element, n_elements):
        return first_element * n_elements * (n_elements + 1) / 2
    
    @classmethod
    def log(cls, message, level=lg.INFO):
        cls.logger.log(level, message)
