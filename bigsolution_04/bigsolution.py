__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'


def divide(limit, number):
    return (limit - 1) // number


def pa_sum(first_element, n_elements):
    return first_element * n_elements * (n_elements + 1) / 2


class BigSolutionError(Exception):
    pass


class BigSolution(object):
    
    FIRST_NUMBER = 3
    SECOND_NUMBER = 5
    
    def __init__(self):
        super().__init__()
    
    def find_sum_of_multiples(self, limit):
        first_number_multiples_sum = BigSolution._number_multiples_sum(
            limit, BigSolution.FIRST_NUMBER
        )
        
        second_number_multiples_sum = BigSolution._number_multiples_sum(
            limit, BigSolution.SECOND_NUMBER
        )
        
        product = BigSolution.FIRST_NUMBER * BigSolution.SECOND_NUMBER
        product_multiples_sum = BigSolution._number_multiples_sum(
            limit, product
        )
        
        result = (
                first_number_multiples_sum
                + second_number_multiples_sum
                - product_multiples_sum
        )
        
        return result
    
    @staticmethod
    def _number_multiples_sum(limit, number):
        return pa_sum(number, divide(limit, number))
