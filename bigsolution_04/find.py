__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'

from .bigsolution import BigSolution


def find_sum_of_multiples(first_number, second_number, limit):
    big_solution = BigSolution(first_number, second_number)
    result = big_solution.find_sum_of_multiples(limit)
    return result


def find_sum_of_multiples_of_3_and_5_since_to_1000():
    return find_sum_of_multiples(3, 5, 1000)
