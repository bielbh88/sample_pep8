__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'


def find_sum_of_multiples(
        first_number, second_number,
        limit):
    result = 0
    for n in range(limit):
        if (first_number % n == 0
                or second_number % n == 0):
            result += n
    return result


x = find_sum_of_multiples(
    3, 5,
    1000)
print(x)


def find_sum_of_multiples(
    first_number, second_number,
    limit):
    result = 0
    for n in range(limit):
        if (first_number % n == 0
            or second_number % n == 0):
            result += n
    return result


x = find_sum_of_multiples(3, 5,
    1000)
print(x)
