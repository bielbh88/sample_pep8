__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'


def find_sum_of_multiples(first_number, second_number, limit):
    n = (limit-1) // first_number
    first_number_multiples_sum = first_number * n * (n+1) / 2
    
    n = (limit-1) // second_number
    second_number_multiples_sum = second_number * n * (n+1) / 2
    
    product = first_number * second_number
    n = (limit-1) // product
    product_multiples_sum = product * n * (n+1) / 2
    
    result = (
        first_number_multiples_sum
        + second_number_multiples_sum
        - product_multiples_sum
    )
    return result


x = find_sum_of_multiples(
    3, 5,
    1000)
print(x)
