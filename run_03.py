__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'


def divide(limit, number):
    return (limit-1) // number


def pa_sum(first_element, n_elements):
    return first_element * n_elements * (n_elements+1) / 2


def find_sum_of_multiples(first_number, second_number, limit):
    first_number_multiples_sum = pa_sum(
        first_number,
        divide(limit, first_number))
    second_number_multiples_sum = pa_sum(
        second_number,
        divide(limit, second_number))
    
    product = first_number * second_number
    product_multiples_sum = pa_sum(
        product, divide(limit, product))
    
    result = (
        first_number_multiples_sum
        + second_number_multiples_sum
        - product_multiples_sum
    )
    return result


x = find_sum_of_multiples(
    3, 5,
    1000)
print(x)
