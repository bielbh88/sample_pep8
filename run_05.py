__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'

import os
import sys
from time import time, sleep
import logging as lg
import threading as th

import numpy as np
from scipy import special as sp
from matplotlib import pyplot as plt
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error

from bigsolution import (
    BigSolution,
    find_sum_of_multiples_of_3_and_5_since_to_1000
)


os, sys, time, sleep, lg, th, np, sp, plt, pd, RandomForestRegressor
mean_squared_error, mean_absolute_error, BigSolution
find_sum_of_multiples_of_3_and_5_since_to_1000

logger = lg.getLogger('MyLogger')
logger.info('Here is an example: "I want coffee."')
logger.info("An another: 'I want vodka.'")

logger.info("""But on multi lines,
use always double quote.
""")

logger.info('Here is an example: \'I do not want coffee.\'')
logger.info("And so on: \"I do not like vodka.\"")
logger.info('''And using single quote
on multi lines string just by rebellion.
''')
