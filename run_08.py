__author__ = 'Gabriel Salgado'
__version__ = '0.1.0'

import logging as lg

from bigsolution import find_sum_of_multiples


big = 'big'
solution = 'solution'
print(''.join((big, solution)))
print(f'{big}{solution}')
print(big + solution)


x = find_sum_of_multiples(3, 5, 10)
if x is not None:
    print(x)
if not x is None:
    print(x)


class Complexity(object):
    
    def __init__(self, first_number: int, second_number: int):
        super().__init__()
        self.first_number = first_number
        self.second_number = second_number
        self.numbers = (first_number, second_number)
        self.sum = sum(self.numbers)
    
    def __eq__(self, other): return self.sum == other.sum
    def __ne__(self, other): return self.sum != other.sum
    def __lt__(self, other): return self.sum < other.sum
    def __le__(self, other): return self.sum <= other.sum
    def __gt__(self, other): return self.sum > other.sum
    def __ge__(self, other): return self.sum >= other.sum

print(list(sorted([Complexity(3, 5), Complexity(4, 5), Complexity(5, 7)])))


def select(*numbers_pairs): return min(map(Complexity, numbers_pairs))
select = (lambda *numbers_pais: min(map(Complexity, numbers_pais)))

print(select((3, 5), (4, 5), (5, 7)))


class ComplexityError(Exception):
    pass


class WrongError(BaseException):
    pass


name = 'BigSolution'
if name.startswith('Big'):
    print(name)
if name[:3] == 'Big':
    print(name)


number = '10'
if isinstance(number, int):
    print(number)
if type(number) is int:
    print(number)


result = 0
multiples_of_3 = [3, 6, 9]
if multiples_of_3:
    result += sum(multiples_of_3)
if len(multiples_of_3):
    result += sum(multiples_of_3)


has_coffee = False
if has_coffee:
    print('It is all right')
if has_coffee == True:
    print('It is all right')
if has_coffee is True:
    print('It is all right')


logger = lg.getLogger('BigSolution')
limit = 1000
options = {'a': (3, 5), 'b': (4, 5), 'c': (5, 7)}
chosen = 'a'
try:
    first_number, second_number = options[chosen]
except KeyError as exc:
    logger.exception(f'Unknown option {exc}.')
    raise ComplexityError('Option must be one of {a, b, c}')
else:
    complexity = Complexity(first_number, second_number)
    total = sum(complexity.numbers)
    solution = find_sum_of_multiples(first_number, second_number, limit)


try:
    logger = lg.getLogger('BigSolution')
    limit = 1000
    options = {'a': (3, 5), 'b': (4, 5), 'c': (5, 7)}
    chosen = 'a'
    first_number, second_number = options[chosen]
    complexity = Complexity(first_number, second_number)
    total = sum(complexity.numbers)
    solution = find_sum_of_multiples(first_number, second_number, limit)
except:
    logger.exception(f'Unknown option.')
    raise ComplexityError('Option must be one of {a, b, c}')


class InputError(Exception):
    pass

''
class InputTaker(object):
    
    def __init__(self):
        super().__init__()
        self.first_number = None
        self.second_number = None
        self.has_numbers = False
    
    def __enter__(self):
        return self.take_inputs()
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
    
    def reset(self):
        self.first_number = None
        self.second_number = None
        self.has_numbers = False
    
    def take_inputs(self):
        self.first_number = InputTaker.take_single_int('First number:')
        self.second_number = InputTaker.take_single_int('Second number:')
        self.has_numbers = True
        return self
    
    @staticmethod
    def take_single_int(message: str, retry: int = 3) -> int:
        exception = None
        for retried in range(retry):
            try:
                number = int(input(message))
            except ValueError as exc:
                exception = exc
            else:
                return number
            print('Sorry, but I did not understand.')
        raise InputError(
            f'Impossible to get number. On last retry: {exception}'
        )


with InputTaker().take_inputs() as it:
    print(find_sum_of_multiples(
        it.first_number, it.second_number, limit
    ))

with InputTaker() as it:
    print(find_sum_of_multiples(
        it.first_number, it.second_number, limit
    ))


def run():
    limit = 1000
    logger = lg.getLogger('BigSolution')
    
    with InputTaker().take_inputs() as it:
        try:
            return find_sum_of_multiples(
                it.first_number, it.second_number, limit
            )
        except Exception as exc:
            logger.exception(f'Unexpected exception: {exc}')
            return None


result = run()
if result is not None:
    print(f'Result: {result}')


def run():
    limit = 1000
    logger = lg.getLogger('BigSolution')
    
    with InputTaker().take_inputs() as it:
        try:
            return find_sum_of_multiples(
                it.first_number, it.second_number, limit
            )
        except Exception as exc:
            logger.exception(f'Unexpected exception: {exc}')


result = run()
if result is not None:
    print(f'Result: {result}')


def run():
    result = None
    limit = 1000
    logger = lg.getLogger('BigSolution')

    with InputTaker().take_inputs() as it:
        try:
            result = find_sum_of_multiples(
                it.first_number, it.second_number, limit
            )
        except Exception as exc:
            logger.exception(f'Unexpected exception: {exc}')
            raise exc
        finally:
            return result


result = run()
if result is not None:
    print(f'Result: {result}')
